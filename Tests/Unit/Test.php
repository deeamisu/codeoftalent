<?php declare(strict_types=1);
/**
 * Created by PhpStorm.
 * User: Misu
 * Date: 11/20/2020
 * Time: 5:13 AM
 */

require (dirname(dirname(__FILE__))). "/../vendor/autoload.php";
use App\Ticket;
use PHPUnit\Framework\TestCase;

final class Test extends TestCase
{

    public function testSame():void
    {
        $tickets = [
            ['from' => 'B', 'to' => 'C', 'info' => '', 'details' => ''],
            ['from' => 'A', 'to' => 'B', 'info' => '', 'details' => ''],
        ];
        $procesedTickets = [
            ['from' => 'A', 'to' => 'B', 'info' => '', 'details' => ''],
            ['from' => 'B', 'to' => 'C', 'info' => '', 'details' => ''],
        ];

        $this->assertSame($procesedTickets, sortArray($tickets));
    }

    public function testEqual():void
    {
        $tickets = [
            ['from' => 'B', 'to' => 'C', 'info' => '', 'details' => ''],
            ['from' => 'C', 'to' => 'D', 'info' => '', 'details' => ''],
            ['from' => 'A', 'to' => 'B', 'info' => '', 'details' => ''],
        ];
        $procesedTickets = [
            ['from' => 'A', 'to' => 'B', 'info' => '', 'details' => ''],
            ['from' => 'B', 'to' => 'C', 'info' => '', 'details' => ''],
            ['from' => 'C', 'to' => 'D', 'info' => '', 'details' => ''],
        ];

        $this->assertEquals($procesedTickets, sortArray($tickets));
    }

    public function testSameObject()
    {
        $tickets = [
            ['from' => 'B', 'to' => 'C', 'info' => '', 'details' => ''],
            ['from' => 'A', 'to' => 'B', 'info' => '', 'details' => ''],
        ];
        $ticket = new Ticket($tickets);
        $procesedTickets = [
            ['from' => 'A', 'to' => 'B', 'info' => '', 'details' => ''],
            ['from' => 'B', 'to' => 'C', 'info' => '', 'details' => ''],
        ];
        $this->assertSame($procesedTickets, $ticket->sortTickets());
    }

    public function testEqualObject():void
    {
        $tickets = [
            ['from' => 'B', 'to' => 'C', 'info' => '', 'details' => ''],
            ['from' => 'C', 'to' => 'D', 'info' => '', 'details' => ''],
            ['from' => 'A', 'to' => 'B', 'info' => '', 'details' => ''],
        ];
        $ticket = new Ticket();
        $ticket->setTickets($tickets);
        $procesedTickets = [
            ['from' => 'A', 'to' => 'B', 'info' => '', 'details' => ''],
            ['from' => 'B', 'to' => 'C', 'info' => '', 'details' => ''],
            ['from' => 'C', 'to' => 'D', 'info' => '', 'details' => ''],
        ];
        $this->assertEquals($procesedTickets, $ticket->sortTickets());
    }
}
