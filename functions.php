<?php

//Sorintg algorithm
function sortArray($tickets)
{
    $processedTickets = [];
    $processedTickets[] = array_shift($tickets);

    while (count($tickets) > 0) {
        for ($key = 0; $key < count($tickets); $key++) {
            if ($tickets[$key]['to'] == $processedTickets[0]['from']) {
                array_unshift($processedTickets, $tickets[$key]);
                unset($tickets[$key]);
                $tickets = array_values($tickets);
                $key--;
            }
            elseif ($tickets[$key]['from'] == $processedTickets[count($processedTickets) -1]['to']) {
                array_push($processedTickets, $tickets[$key]);
                unset($tickets[$key]);
                $tickets = array_values($tickets);
                $key--;
            }
        }
    }

    return $processedTickets;
}
