<?php
/**
 * Created by PhpStorm.
 * User: Misu
 * Date: 11/21/2020
 * Time: 6:16 AM
 */

namespace App;

require (dirname(__FILE__)). "/../vendor/autoload.php";

trait TicketUtilities
{
    public function sortThisArray($arrayToSort)
    {
        return sortArray($arrayToSort);
    }
}