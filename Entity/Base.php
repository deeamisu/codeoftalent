<?php

namespace App;

abstract class Base
{
    /** @var array  */
    protected $tickets;

    /**
     * Base constructor.
     * @param $tickets array
     */
    public function __construct($tickets = [])
    {
        $this->tickets = $tickets;
    }

    /**
     * @return mixed
     */
    public function getTickets()
    {
        return $this->tickets;
    }

    /**
     * @param mixed $tickets
     */
    public function setTickets($tickets)
    {
        $this->tickets = $tickets;
    }

    abstract public function sortTickets();
}