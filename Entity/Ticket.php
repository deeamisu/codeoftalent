<?php
/**
 * Created by PhpStorm.
 * User: Misu
 * Date: 11/21/2020
 * Time: 6:09 AM
 */

namespace App;

class Ticket extends Base
{
    use TicketUtilities;

    public function sortTickets()
    {
        return $this->sortThisArray($this->getTickets());
    }

    /** @param $tickets array */
    public static function message($tickets)
    {
        $text = '';
        foreach ($tickets as $ticket) {
            $message = "De la " . $ticket['from'] . " catre " . $ticket['to'] . ' ' . $ticket['details'] . ' ' . $ticket['info']."<br>";
            $text = $text . $message;
        }
        return $text;
    }
}