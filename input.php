<?php

$tickets = [
     [
        'from' => 'Pitesti' ,
        'to' => 'Brasov',
        'info' => 'vagonul 3 loc 2',
        'details' => 'luam trenul',
],
     [
        'from' => 'Turnu-Severin' ,
        'to' => 'Timisoara',
        'info' => '',
        'details' => 'cu avionul',
    ],
     [
        'from' => 'Cluj' ,
        'to' => 'Bacau',
        'info' => 'cu bagaje in carca',
        'details' => 'mers pe jos',
    ],
     [
        'from' => 'Timisoara' ,
        'to' => 'Constanta',
        'info' => '',
        'details' => '',
    ],
    [
        'from' => 'Bucuresti' ,
        'to' => 'Pitesti',
        'info' => 'de 4 locuri',
        'details' => 'cu masina',
    ],
    [
        'from' => 'Brasov' ,
        'to' => 'Cluj',
        'info' => '',
        'details' => '',
    ],
    [
        'from' => 'Bacau' ,
        'to' => 'Turnu-Severin',
        'info' => '',
        'details' => '',
    ],
];

$data = json_encode($tickets);

$url = 'http://188.240.210.8/workgroup00/mihai/CodeOfTalent/process.php';
$ch = curl_init($url);
curl_setopt($ch, CURLOPT_POST, 1);
curl_setopt($ch, CURLOPT_POSTFIELDS, $data);
curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: application/json'));
$result = curl_exec($ch);

//var_dump($result);