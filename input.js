/**
 * Created by Misu on 11/19/2020.
 */
function sendJSON(){

    var tickets = [
        {
            'from': 'Cluj',
            'to':'Brasov',
            'info':'',
            'details':''
        },
        {
            'from': 'Sibiu',
            'to':'Alba',
            'info':'',
            'details':''
        },
        {
            'from': 'Alba',
            'to':'Cluj',
            'info':'',
            'details':''
        }
    ];

    // Creating a XHR object
    var xhr = new XMLHttpRequest();
    var url = "http://188.240.210.8/workgroup00/mihai/CodeOfTalent/process.php";

    // open a connection
    xhr.open("POST", url, true);

    // Set the request header i.e. which type of content you are sending
    xhr.setRequestHeader("Content-Type", "application/json");

    // Create a state change callback
    xhr.onreadystatechange = function () {
        if (xhr.readyState === 4 && xhr.status === 200) {

            var result = document.querySelector('#result');

            // Print received data from server
            result.innerHTML = this.responseText;

        }
    };

    // Converting JSON data to string
    var data = JSON.stringify(tickets);

    // Sending data with the request
    xhr.send(data);

    console.log(data);
}
