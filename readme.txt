    For installing the app , you have to copy Entity folder, composer.json, process.php and functions.php files to
server, and then send a POST request as JSON array , wich contains JSON objects with keys 'to' , 'from' , 'info' ,
'details' and associated values.
    Run composer install comand in your CLI.
    If you have previously runned this app, in earlier version, remember to run composer dumpautoload command.
    For testing the app, you can run php/vendor/bin/phpunit in your CLI and see test results. More tests can be
written in Tests\Unit\Test.php class.